/**
 * @file калькулятор.c
 * @author your name
 * @brief 
 * @version 0.1
 * @date 2021-09-19
 * 
 * @copyright Copyright (c) 2021
 * 
 */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int main()
{
    char simbol;
    int rez,number,isimbol;
    FILE *task;

    task=fopen("calculator.txt","r");
    
    rez=0;
    number=0;

    if (task==NULL)
    {
        printf("%s", "ERROR");
    }

    simbol = getc (task);
    while ((((simbol) != '+')&&((simbol) != '-'))&&(simbol != EOF))
        {
            isimbol=simbol - '0';
            number=number*10 +isimbol;
            simbol = getc(task);
        }
    rez=number;
    while (simbol != EOF)
    {
        if (simbol == '+')
        {
            simbol = getc(task);
            number=0;
            while ((((simbol) != '+')&&((simbol) != '-'))&&(simbol != EOF))
            {
                isimbol=simbol - '0';
                number=number*10 +isimbol;
                simbol = getc (task);
            }
            rez=rez+number;
        }
        if (simbol == '-')
        {
            simbol = getc(task);
            number=0;
            while ((((simbol) != '+')&&((simbol) != '-'))&&(simbol != EOF))
            {
                isimbol=simbol - '0';
                number=number*10 +isimbol;
                simbol = getc (task);
            }
            rez=rez-number;
        }
    }
    printf ("%d\n",rez);
    printf ("DONE\n");
    fclose(task);
}