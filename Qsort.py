def QSort (a,left, right):
    key = a[(left+right)//2]
    i = left
    j = right
    while True:
        while a[i]<key and i<len(a):
            i+=1
        while key<a[j] and j>=0:
            j-=1
        if i<=j:
            a[i], a[j] = a[j], a[i]
            i+=1
            j-=1
        if i>j:
            break
    if left<j:
        QSort (a,left,j)
    if i<right:
        QSort(a,i,right)

with open ("sort.in","r") as f:
    n=int (f.readline())
    a=[0]*n
    i=0
    for j in range (n):
        a[i]= list(map(str,f.readline().split()))
        i+=1
    QSort(a,0,n-1)
with open ("sort.out","w") as f1:
    f1.write(str(a))